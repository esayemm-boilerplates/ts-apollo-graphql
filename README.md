# Apollo GraphQL Server Boilerplate

Apollo GraphQL Server that is built on top of: Typescript, Koa, Apollo

# Getting Started

Run `yarn run dev` then visit [http://localhost:8081/graphql](http://localhost:8081/graphql)

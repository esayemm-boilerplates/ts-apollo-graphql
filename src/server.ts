import Koa from 'koa'
import bodyParser from 'koa-bodyparser'
import cors from '@koa/cors'

import logger from 'logger'
import config from 'config'
import graphqlServer from 'graphql/server'

export default function start(): void {
  const app = new Koa()

  // Hook up middlewares
  app.use(bodyParser())
  app.use(cors())

  // Hook up graphql to the http server
  graphqlServer.applyMiddleware({ app })

  // Spin up!
  app.listen(
    config.PORT,
    (): void => {
      logger.log('info', `Server listening on port ${config.PORT}`)
    },
  )
}

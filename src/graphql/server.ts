import { ApolloServer, gql } from 'apollo-server-koa'
import * as Koa from 'koa'
import { merge } from 'lodash'

import {
  resolver as helloWorldResolver,
  typeDef as helloWorldTypeDef,
} from './entities/helloWorld'

// https://blog.apollographql.com/modularizing-your-graphql-schema-code-d7f71d5ed5f2
const root = gql`
  type Query {
    root: String
  }

  type Mutation {
    root: String
  }
`

export default new ApolloServer({
  context: async ({ ctx }: { ctx: Koa.Context }): Promise<{}> => {
    console.log(`derive context from ${ctx.request}`)
    return {}
  },
  resolvers: merge(helloWorldResolver),
  typeDefs: [root, helloWorldTypeDef],
})

import { gql } from 'apollo-server-koa'

export const typeDef = gql`
  extend type Query {
    helloWorld: String!
  }
`

export const resolver = {
  Query: {
    helloWorld: async (): Promise<string> => {
      return 'Hello World'
    },
  },
}
